<?php

namespace App\Repositories;

use DB;
use App\BankAccount;

class BankAccountRepository
{
   public function get_bankaccount()
    {
        $p = BankAccount::orderBy('created_at', 'asc')
                    ->get();
        return $p;
    }
    // public function get_bankaccount_by_user( $user_id ) {
    //     $p = BankAccount::where('user_id', $user_id )
    //                 ->get();
    //     return $p[0];
    // }

    public static function forUser( $user_id )
    {
        return BankAccount::where('user_id', $user_id )
                    ->orderBy('created_at', 'asc')
                    ->get();
    }

    public static function get_bankaccount_by_id( $id ) {
        $p = BankAccount::where('id', '=', $id )->get();
        return $p[0];
    }


    //task 4
    public static function get_jointAccount( $user_id ) {
        $p = BankAccount::where('user_id', '!=', $user_id )->where('type', '=', 'joinAccount' )->get();
        return $p;
    }

    public function get_jointAccount_by_joiner( $user_id ) {
        $joinAccount = array();
        $accounts = $this->get_bankaccount();
        foreach ( $accounts as $account ) {
            $assoc = unserialize( $account->associated );
            if ( $assoc ) {
               if ( in_array( $user_id , $assoc ) ) {
                   $joinAccount[] = $account;
               }
            }
        }
        return $joinAccount;
    }
}
