<?php
namespace App;

use DB;
use App;
use App\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'user_name', 'email', 'password', 'role_id', 'twitter_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    // role
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    public static function get_user_role_id( $user_id ) {
        $role_id = User::where( 'id', $user_id )->get();
        return $role_id[0]->role_id;
    }

    public static function get_role( $user_id )
    {
        $role_id = User::get_user_role_id( $user_id );
        $role = Role::where( 'id' , $role_id)->get();
        return $role[0]->role_name;
    }

    // get user info
   
}