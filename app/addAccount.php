<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\task;

class addAccount extends task
{

	public $type = 'addAccount';

    public $view = 'addAccount';

    public $text = 'Add Account';

    public $banks = array( 'Barclays', 'Lloyd', 'Natwest', 'TSB' );

    public $icon = 'fa-user';
}
