<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BankAccount;

class ISA extends BankAccount
{
    protected $table = 'accounts';

    function get_type() {
		return 'ISA';
	}
}
