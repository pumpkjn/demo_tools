<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\payment_method;

class directDebit extends payment_method
{
    public $method = 'direct';
}
