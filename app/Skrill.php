<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skrill extends payment_method
{
    public $method = 'skrill';
}
