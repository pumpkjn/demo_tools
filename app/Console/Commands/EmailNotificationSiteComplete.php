<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Site;
use App\User;
use Mail;


class EmailNotificationSiteComplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Mail when a site complete';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $sites = Site::where( 'status' , '=', 'installed')->get();
        foreach ( $sites as $site ) {
            $data = array();
            $params = unserialize( $site->params );
            $user_mail = $params['user_email'];
            

            Mail::send('emails.site_complete', $data ,function ( $m ) use ( $user_mail ) {
                $m->from('JoomLa', 'Your Sites');

                $m->to( $user_mail , 'something')->subject('Install Complete');
            });

            if(count(Mail::failures()) > 0) {
               $alt_status = 'installed';
            } else {
                $alt_status = 'completed';
            }

            Site::update_site_status( $site, $alt_status );
        }
    }
}
