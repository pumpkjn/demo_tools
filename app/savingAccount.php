<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BankAccount;

class savingAccount extends BankAccount
{
    //overide
    function get_type() {
		return 'saving';
	}
}
