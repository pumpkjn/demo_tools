<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BankAccount;

class checkingAccount extends BankAccount
{
    protected $table = 'accounts';

    //overide
    function get_type() {
		return 'checking';
	}
}
