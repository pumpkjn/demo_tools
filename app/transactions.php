<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transactions extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['user_id', 'bank_id' ,'amount', 'from', 'to', 'type', 'balance', 'method'];

	public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function create_transaction( $user_id, $bank_id ,$amount, $from, $to, $type, $balance, $method ) {
		$this->create([
         	'user_id' => $user_id,
         	'bank_id' => $bank_id,
            'amount' => $amount,
            'from' => $from,
            'to' => $to,
            'type' => $type,
            'balance' => $balance,
            'method' => $method,
        ]);
    }
}
