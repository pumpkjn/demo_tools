<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmailCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $current_user_email = $request->user()->email;
            if ( '' == $current_user_email || null == $current_user_email ) {
                return redirect('user/email');
            } else {
                return $next($request);
            }
            
        }
        
    }
}
