<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:3',
            'twitter_id' => '',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $dataname
     * @return User
     */
    protected function create(array $data)
    {   
        if ( !isset( $data['twitter_id'] ) ) {
            $data['twitter_id'] = '';
        }
        return User::create([
            'role_id' => 3,
            'user_name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'twitter_id' => $data['twitter_id'],
        ]);
    }

    public function redirectToFacebookProvider() {
        return Socialite::driver( 'facebook' )->redirect();
    }

    public function handelFacebookProvider() {
        $user = Socialite::driver( 'facebook' )->user();

        $email = isset( $user->email ) ? $user->email : '';

        $data = ['name' => $user->name, 'email' => $email, 'password' => $user->token, 'twitter_id' => ''];
        $userDB = User::where( 'email', $user->email )->first();
        if ( !is_null( $userDB ) ) {
            Auth::login( $userDB );
        } else {
            Auth::login( $this->create( $data ) );
        }

        return redirect('/');
    }

    public function redirectToGooglePlusProvider() {
        return Socialite::driver( 'google' )->redirect();
    }

    public function handelGooglePlusProvider() {

        if ( isset( $_GET['error'] ) ) {
            return redirect('/');
        } else {
            $user = Socialite::driver( 'google' )->user();

            $data = ['name' => $user->name, 'email' => $user->email, 'password' => $user->token, 'twitter_id' => ''];
            $userDB = User::where( 'email', $user->email )->first();
            if ( !is_null( $userDB ) ) {
                Auth::login( $userDB );
            } else {
                Auth::login( $this->create( $data ) );
            }

            return redirect('/');
        }
    }

    public function redirectToTwitterProvider() {
        return Socialite::driver( 'twitter' )->redirect();
    }

    public function handelTwitterProvider() {

        if ( isset( $_GET['error'] ) ) {
            return redirect('/');
        } else {
            $user = Socialite::driver( 'twitter' )->user();

            $data = ['name' => $user->name, 'twitter_id' => $user->id, 'password' => $user->token, 'email' => ''];
            $userDB = User::where('twitter_id', $user->id)->first();
            if ( !is_null( $userDB ) ) {
                Auth::login( $userDB );
            } else {
                Auth::login( $this->create( $data ) );
            }

            return redirect('/');
        }
    }
}
