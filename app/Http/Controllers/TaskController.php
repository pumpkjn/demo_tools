<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankAccountRepository;
use App\Repositories\TransactionRepository;
use App\jointAccount;
use App\BankAccount;
use App\transactions;
use App\payment_method;
use App\task;
use App\topup;
use App\addAccount;
use App\joint;

class TaskController extends Controller
{
	
	public function addAccount( Request $request,BankAccount $account, addAccount $task ) {

		return view('guest.bankaccount.task', [
			'user' => $request->user(),
			'task' => $task,
			'account_types' => $account->types,
		]);

	}


	public function add( Request $request,BankAccount $account, addAccount $task ) {
		$bank = $request->input('bank');
		$type = $request->input('type');
		$sortcode = $request->input('sortcode');
		$accountnumber = $request->input('accountnumber');
		$IBAN = $request->input('iban');
		$account->add_account( $request->user()->id, $bank, $type, $sortcode, $accountnumber, $IBAN );
		return redirect('/bankaccount');
	}

	public function topup( Request $request,BankAccount $account, topup $task, BankAccountRepository $Bankrepo,  payment_method $payment_method ) {

		return view('guest.bankaccount.task', [
			'user' => $request->user(),
			'task' => $task,
			'bankrepo' => $Bankrepo->forUser( $request->user()->id ),
			'payment_method' => $payment_method->methods,
		]);

	}

	public function up( Request $request,BankAccount $account, addAccount $task, transactions $transactions) {
		$account = $request->input('account');
		$amount = $request->input('amount');
		$method = $request->input('method');

		$bankAccount = BankAccount::where('id', $account)->first();
               
		if ( $amount && $amount >= $bankAccount->balance ) {
            return redirect('/bankaccount/task/topup?status=over_amount_topup');
        } else if ( $amount ) {

        	$new_balance = $bankAccount->withdraw( $amount );
            $transactions->create_transaction( $request->user()->id, $bankAccount->id ,$amount, null, null , 'topup', $new_balance, $method );
            return redirect('/bankaccount/account/'.$bankAccount->id);
           
        } else {
            return redirect('/bankaccount/task/topup?status=topup_fail');
        }
	}

	// task 4

	public function jointIndex( Request $request,BankAccount $account, joint $task, BankAccountRepository $Bankrepo ) {

		return view('guest.bankaccount.task', [
			'user' => $request->user(),
			'task' => $task,
			'bankrepo' => $Bankrepo->get_jointAccount( $request->user()->id ),
		]);
	}

	public function joint( Request $request,jointAccount $account, joint $task, BankAccountRepository $Bankrepo ) {
		$account = $request->input('account');

		$bankAccount = jointAccount::where('id', $account)->first();
		$bankAccount->associated( $request->user()->id );

		return redirect('/bankaccount/account/'.$account);

	}
}
