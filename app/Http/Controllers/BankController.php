<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankAccountRepository;
use App\Repositories\TransactionRepository;
//task 4
use App\Repositories\UserRepository;
use App\BankAccount;
use App\transactions;
use App\payment_method;
use App\task;
use App\topup;
use App\addAccount;

class BankController extends Controller
{
    protected $accounts;

     public function __construct(BankAccountRepository $accounts)
    {

        $this->accounts = $accounts;

    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index( Request $request,BankAccountRepository $accounts,task $task )
    {   
        return view('guest.bankaccount.index', [
            'user' => $request->user(),
            'accounts' => $this->accounts->forUser($request->user()->id),
            'task' => $task,
            // task 4
            'jointAccounts' => $accounts->get_jointAccount_by_joiner( $request->user()->id ),
        ]);
    }

    public function account( Request $request,BankAccount $account,TransactionRepository $transactions,BankAccountRepository $bankrepo, payment_method $payment_method, UserRepository $userRepo)
    {   
        return view('guest.bankaccount.account', [
            'account' => $account,
            'transactions'=> $transactions->get_transactions( $request->user()->id, $account->id ),
            'bankrepo' => $bankrepo->get_bankaccount(),
            'payment_method' => $payment_method->methods,
            // task 4
            'userRepo' => $userRepo,
        ]);
    }

    public function deposit( Request $request,BankAccount $account, transactions $transactions )
    {   

        $amount = $request->input('deposit');
        if ( $amount ) {
           $new_balance = $account->deposit( $amount );
            $transactions->create_transaction( $request->user()->id, $account->id ,$amount, null, null , 'deposit', $new_balance, null );
            return redirect('/bankaccount/account/'.$account->id);
        } else {
            return redirect('/bankaccount/account/'.$account->id. '?status=deposit_fail');
        }
    }


    public function withdraw( Request $request,BankAccount $account, transactions $transactions )
    {   

        $amount = $request->input('withdraw');

        if ( $amount && $amount <= $account->balance ) {
           $new_balance = $account->withdraw( $amount );
            $transactions->create_transaction( $request->user()->id, $account->id,$amount, null, null , 'withdraw', $new_balance, null );
            return redirect('/bankaccount/account/'.$account->id);
        } else if ( $amount && $amount >= $account->balance ) {
            return redirect('/bankaccount/account/'.$account->id. '?status=over_amount');
        } else {
            return redirect('/bankaccount/account/'.$account->id. '?status=withdraw_fail');
        }
    }


    public function transfer( Request $request,BankAccount $account, transactions $transactions )
    {   

        $amount = $request->input('transfer');
        $trans_bank = $request->input('trans-bank');
        $method = $request->input('method');
        if ( $amount && $amount <= $account->balance ) {

            // minus from current bank
           $res = $account->transfer( $amount, $trans_bank );



            $transactions->create_transaction( $res[0]['user_id'], $account->id,$amount, null, $trans_bank , 'transfer', $res[0]['balance'], $method );
            $transactions->create_transaction( $res[1]['user_id'], $trans_bank,$amount, $account->id, null , 'receive', $res[1]['balance'], $method );

            return redirect('/bankaccount/account/'.$account->id);

        } else if ( $amount && $amount >= $account->balance ) {

            return redirect('/bankaccount/account/'.$account->id. '?status=over_amount_transfer');
        } else {
            return redirect('/bankaccount/account/'.$account->id. '?status=transfer_fail');
        }
    }

    // public function addAccount( Request $request,BankAccountRepository $accounts )
    // {   
    //     return view('guest.bankaccount.index', [
    //         'user' => $request->user(),
    //         'accounts' => $this->accounts->forUser($request->user()->id),
    //     ]);
    // }
}
