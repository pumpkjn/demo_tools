<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\task;

class topup extends task
{
	public $type = 'topup';

    public $view = 'topup';

    public $text = 'Mobile Topup';

    public $icon = 'fa-phone';
}
