<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\payment_method;

class Paypal extends payment_method
{
    public $method = 'paypal';
}
