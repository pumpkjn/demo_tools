To set up this laravel Project:
Step 1: in database create a database name ha07
Step 2: the database info is in .env file, the current settings are :
	DB_HOST=localhost
	DB_DATABASE=ha07
	DB_USERNAME=ha07
	DB_PASSWORD=ha07
as stated in the course work specification

Step 3 is import the database : can be execute by 2 ways
1. Manual database import : SQL file is in folder: sql/ha07.sql
-----------------------------------------
2. Advance option with Composer, first, you need to install composer from : https://getcomposer.org/
Next, navigate inside the comp1605 folder, use CMD to run : "php artisan migrate:refresh" then "php artisan db:seed".



--------------
Apache and SQL server:
I'm currently using Xampp from https://www.apachefriends.org/index.html.
Download xamp and place the folder comp1607 inside xampp/htdocs
-------------
Database Migrate and Seed are in folders : database/migrations and database/seed.
Database migration contain structure of all tables
Database seed contain demo content to fill in the tables

------ 
The zip file have large size due to all frameworks being zipped. All the Frameworks support laravel in vendor folder should be installed through Composer by "composer install"

