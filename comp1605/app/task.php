<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\addAccount;
use App\topup;
use App\joint;

class task extends Model
{
    public $tasks = array( 'addAccount', 'topup', 'joint');

    public function get_view( $task ) {
    	return $this->view;
    }

    public function create_task_class( $type ) {
    	switch ( $type ) {
    		case 'addAccount':
    			return new addAccount();
    			break;
    		
    		case 'topup':
    			return new topup();
    			break;

    		case 'joint':
    			return new joint();
    			break;
    	}
    }

    public function get_text() {
    	return $this->text;
    } 
}
