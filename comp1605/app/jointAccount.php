<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jointAccount extends BankAccount
{
    protected $table = 'accounts';

    protected $fillable = ['user_id', 'balance' ,'sortcode', 'branchname', 'accountnumber', 'IBAN', 'type', 'associated'];

    public function get_type() {
		return 'jointAccount';
	}

	public function associated( $user ) {
		$soc_array = array();
		$current_associated = $this->associated;
		if ( !$current_associated ) {
			$soc_array[] = $user;
		} else {
			$soc_array = unserialize( $current_associated );
			$soc_array[] = $user;
		}
		$this->associated = serialize( $soc_array );
		$this->save();
	}
}
