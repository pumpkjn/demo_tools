<?php

namespace App\Repositories;

use DB;
use App\User;

class UserRepository
{
    public static function get_all_user_except( $user_id ) {
        $users = User::where('id', '!=', $user_id )->get();

        return $users;
    }

    public static function get_all_users() {
        $users = User::orderBy('id', 'asc')
                    ->get();

        return $users;
    }

    public static function get_user_by_id( $user_id ) {
    	$users = User::where('id', '=', $user_id )->get();
    	return $users[0];
    }

}
