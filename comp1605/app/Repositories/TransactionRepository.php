<?php

namespace App\Repositories;

use DB;
use App\BankAccount;
use App\transactions;

class TransactionRepository
{
   // public function get_bankaccount()
   //  {
   //      $t = BankAccount::orderBy('created_at', 'asc')
   //                  ->get();
   //      return $t;
   //  }
    public function get_transactions( $user_id, $bank_id ) {
        $t = transactions::where('user_id', $user_id )
                    ->where('bank_id', $bank_id)
                    ->get();
        return $t;
    }
}
