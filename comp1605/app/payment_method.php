<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_method extends Model
{

    public $methods = array( 'direct', 'paypal', 'skrill' );
}
