<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Repositories\PackageRepository;
use App\Repositories\SiteRepository;
use App\Repositories\UserRepository;
use App\Site;
use App\Package;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    })->middleware('guest');

    // Route for user
    // bank account index pages
    Route::get('/bankaccount', 'BankController@index')->middleware('auth')->middleware('email_check');

    // bank account detail
    Route::get('/bankaccount/account/{account}', 'BankController@account')->middleware('auth')->middleware('own');;
    // deposit
    Route::post('/bankaccount/account/deposit/{account}', 'BankController@deposit')->middleware('auth');
    // withdraw
    Route::post('/bankaccount/account/withdraw/{account}', 'BankController@withdraw')->middleware('auth');
    // transfer
    Route::post('/bankaccount/account/transfer/{account}', 'BankController@transfer')->middleware('auth');


    // bank account tasks

    Route::get('/bankaccount/task/addAccount', 'TaskController@addAccount')->middleware('auth');
    Route::post('/bankaccount/task/addAccount', 'TaskController@add')->middleware('auth');


    Route::get('/bankaccount/task/topup', 'TaskController@topup')->middleware('auth');
    Route::post('/bankaccount/task/topup', 'TaskController@up')->middleware('auth');

    Route::get('/bankaccount/task/joint', 'TaskController@jointIndex')->middleware('auth');
    Route::post('/bankaccount/task/joint', 'TaskController@joint')->middleware('auth');

  
    // Route for auth
    Route::auth();








});
