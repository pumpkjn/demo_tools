<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleChecking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   
        if (Auth::guard($guard)->check()) {
            $current_user_role = $request->user()->get_role( Auth::user()->id );
            if ( 'admin' == $current_user_role || 'manager' == $current_user_role ) {
                return $next($request);
            } else if ( 'guest' == $current_user_role ) {
                return response('Forbidden.', 403);
            }
            
        }
    }
}
