<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard return 
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            // $current_user_role = $request->user()->get_role( Auth::user()->id );

            // if ( 'admin' == $current_user_role ) {
                return redirect('/bankaccount');
            // } else if ( 'user' == $current_user_role ) {
                // return redirect('/bankaccount');
            // }
        }

        return $next($request);
    }
}
