<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Role;
use App\Permission;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a list of all of the user and manage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request )
    {
        return view('guest.users.index', [
       		'sites' => $this->sites->forUser($request->user()),
        ]);
    }

    public function update_email(Request $request) {
        return view('emails.update_user_email', [
            // 'sites' => $this->sites->forUser($request->user()),
        ]);
    }

    public function store_update_email(Request $request){
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
        ]);

        $email = $request->input('email');

        $request->user()->email = $email;
        $request->user()->save();

        return redirect('/');

    }

}
