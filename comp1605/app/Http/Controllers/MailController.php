<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;
use App\User;
use Mail;

class MailController extends Controller
{
	public function site_check( Request $request )
    {
    	$ar = array();
    	$user_id = $request->user()->id;
       	if ( isset( $_GET['action'] )) {
       		if ( 'check_status' == $_GET['action'] ) {
       			$user_role = User::get_role( $user_id );
       			if ( 'manager' == $user_role || 'admin' == $user_role ) {
       				$sites = Site::where( 'status', '!=', 'completed' )->get();
       			} else {
       				$sites = Site::where('user_id' , '=', $user_id )->where( 'status', '!=', 'completed' )->get();
       			}
       			
       			if ( $sites && count($sites) > 0  ) {
       				foreach ( $sites as $site ) {
       					$ar += array( 
		        			$site->id => $site->status,
		        		);

		        		// --- Action mailsend when check a  ---
			        	// if ( 'installed' != $site->status ) {
			        		// $ar += array( 
			        		// 	$site->id => $site->status,
			        		// );
			        	// } else {
			        	// 	if ( 'installed' == $site->status ) {
				                // $data = array();
				                // $params = unserialize( $site->params );
				                // $user_mail = $params['user_email'];
				                

				                // Mail::send('emails.site_complete', $data ,function ( $m ) use ( $user_mail ) {
				                //     $m->from('Joomla', 'Your Site');

				                //     $m->to( $user_mail , 'something')->subject('Install Complete');
				                // });

				                // if(count(Mail::failures()) > 0) {
				                //    $alt_status = 'installed';
				                // } else {
				                // 	$alt_status = 'completed';
				                // }

				                // Site::update_site_status( $site, $alt_status );
				          //       $ar += array( 
				        		// 	$site->id => $alt_status,
				        		// ); 
				        	// }
				        // }
				        // -------------------------------------
			        }
			        echo json_encode($ar);
	   			} else {
	   				echo 'empty';
	   			}
       		}
       	}
	}

	// public function send_mail( Request $request ) {
	// 	$sites = Site::where( 'status' , '=', 'installed')->get();
 //        foreach ( $sites as $site ) {
 //            $data = array();
 //            $params = unserialize( $site->params );
 //            $user_mail = $params['user_email'];
            

 //            Mail::send('emails.site_complete', $data ,function ( $m ) use ( $user_mail ) {
 //                $m->from('pumpkjn1508@gmail.com', 'Your Sites');

 //                $m->to( $user_mail , 'something')->subject('Install Complete');
 //            });

 //            if(count(Mail::failures()) > 0) {
 //               $alt_status = 'installed';
 //            } else {
 //                $alt_status = 'completed';
 //            }

 //            Site::update_site_status( $site, $alt_status );
 //        }
 //        return redirect('/admin/sites');
	// }
}
