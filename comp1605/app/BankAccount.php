<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\transactions;
use App\checkingAccount;
use App\savingAccount;
use App\ISA;
use App\jointAccount;

class BankAccount extends Model
{
	protected $table = 'accounts';

	protected $fillable = ['user_id', 'balance' ,'sortcode', 'branchname', 'accountnumber', 'IBAN', 'type'];

	public $types = array( 'saving', 'checking', 'isa', 'jointAccount' );

	// public function __construct()
	// {
	// 	// $this->type = $this->get_type();
	// }

	public function get_type() {
		return $this->type;
	}

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	public function get_bank_by_id( $id ) {
		$bank = $this::where('id', $id)
               ->first();
        return $bank;   
	}

    public function deposit( $amount ) {

    	$balance = $this->balance;
	   	$new_balance = $balance + $amount;
	   	$this->balance_update( $new_balance );
    	return $new_balance;
    }

    public function withdraw( $amount ) {
    	$balance = $this->balance;
	   	$new_balance = $balance - $amount;
	   	$this->balance_update( $new_balance );
    	return $new_balance;
    }

    private function balance_update( $balance ) {
    	$this->balance = $balance;
    	$this->save();
    }

   	public function transfer( $amount, $account ) {
   		$this->withdraw( $amount );
   		$res[0] = array(
        	'user_id'=> $this->user_id,
        	'amount'=> $amount,
        	'balance'=> $this->balance,
        );

   		// get transfer to account
   		$trans = $this::where('id', $account)
               ->first();

        $trans->deposit( $amount );

        $res[1] = array(
        	'user_id'=> $trans->user_id,
        	'amount'=> $amount,
        	'balance'=> $trans->balance,
        );

        return $res;
   	}

   	public function add_account( $user_id, $branchname, $type, $sortcode, $accountnumber, $IBAN ) {

   		$this->create([
         	'user_id' => $user_id,
            'balance' => 0,
            'sortcode' => $sortcode,
            'branchname' => $branchname,
            'accountnumber' => $accountnumber,
            'IBAN' => $IBAN,
            'type' => $type,
        ]);
   	}

}
