<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\task;

class joint extends task
{
	public $type = 'joint';

    public $view = 'joint';

    public $text = 'Join a joint account';

    public $icon = 'fa-users';
}
