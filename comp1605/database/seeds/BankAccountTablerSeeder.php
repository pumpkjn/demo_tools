<?php

use Illuminate\Database\Seeder;

class BankAccountTablerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // user h07
        DB::table('accounts')->insert([
            'user_id' => 1,
            'balance' => '1000',
            'sortcode' => '101010',
            'branchname' => 'barclays',
            'accountnumber' => '111111111',
            'IBAN' => 'BA123456',
            'type' => 'saving',
            'created_at' => new DateTime
        ]);

        DB::table('accounts')->insert([
            'user_id' => 1,
            'balance' => '2000',
            'sortcode' => '101020',
            'branchname' => 'lloyds',
            'accountnumber' => '222222222',
            'IBAN' => 'LL123456',
            'type' => 'checking',
            'created_at' => new DateTime
        ]);

        // user allen
        DB::table('accounts')->insert([
            'user_id' => 2,
            'balance' => '2000',
            'sortcode' => '202020',
            'branchname' => 'barclays',
            'accountnumber' => '777777777',
            'IBAN' => 'BA777777',
            'type' => 'checking',
            'created_at' => new DateTime
        ]);
        DB::table('accounts')->insert([
            'user_id' => 2,
            'balance' => '2000',
            'sortcode' => '232323',
            'branchname' => 'barclays',
            'accountnumber' => '232323232',
            'IBAN' => 'BA232323',
            'type' => 'ISA',
            'created_at' => new DateTime
        ]);

        // user1

        DB::table('accounts')->insert([
            'user_id' => 3,
            'balance' => '5000',
            'sortcode' => '909090',
            'branchname' => 'barclays',
            'accountnumber' => '999999999',
            'IBAN' => 'BA999999',
            'type' => 'checking',
            'created_at' => new DateTime
        ]);

    }
}
