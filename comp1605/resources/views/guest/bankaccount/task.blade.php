@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					{{ $task->get_text() }}
				</div>
				
					<div class="panel-body">
						<form action="{{ url('/bankaccount/task', $task->type) }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
								@if ( "addAccount" == $task->type)
								
								<div class="form-group">
									<div class="col-md-6">
										Bank:
										<select name="bank">
											@foreach ($task->banks as $bank)
												<option value="{{ $bank }}">{{ $bank }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-6">
										Account type:
										<select name="type">
											@foreach ( $account_types as $type )
													<option value="{{ $type }}">{{ $type }}</option>
											@endforeach
										</select>
										
									</div>
									
								</div>

								<div class="form-group">
									<div class="col-md-6">
										Sortcode:<input name="sortcode" value=""></input>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										Account Number:<input name="accountnumber" value=""></input>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										IBAN:<input name="iban" value=""></input>
									</div>
								</div>
								@elseif ( "topup" == $task->type)
									<div class="form-group">
										<div class="col-md-6">
											Choose Account:
											<select name="account">
												@foreach ( $bankrepo as $bank_account )
													<?php var_dump( $bank_account ); ?>
													<option value="{{ $bank_account->id }}">{{ $bank_account->branchname }} - {{ $bank_account->sortcode }} - {{ $bank_account->accountnumber }} | Balance: {{ $bank_account->balance }}  </option>
												@endforeach
											</select>
										</div>
										<div class="col-md-6">
											<?php $fail = isset( $_GET['status'] ) ? $_GET['status'] : null; ?>
											Amount:
											<input name="amount" value=""></input>
											<?php 
											if ( $fail && 'topup_fail' == $fail ) {
												echo '</br><i>You need to input the topup amount.</i>';
											} ?>
											<?php 
											if ( $fail && 'over_amount_topup' == $fail ) {
												echo '</br><i>Amount of topup need to be lower than your balance.</i>';
											} ?>					
										</div>

										<div class="col-md-6">
											Method:
											<select name="method">
												@foreach ($payment_method as $method)
													<option value="{{ $method }}">{{ $method }}</option>
												@endforeach
											</select>
										
										</div>
									</div>
									@elseif ( "joint" == $task->type)
										<div class="form-group">
										<div class="col-md-6">
											Choose Account:
											<select name="account">
												@foreach ( $bankrepo as $bank_account )
													
													<option value="{{ $bank_account->id }}">{{ $bank_account->branchname }} - {{ $bank_account->sortcode }} - {{ $bank_account->accountnumber }} | Balance: {{ $bank_account->balance }}  </option>
												@endforeach
											</select>
										</div>
									</div>
									@else
									@endif
									<div class="form-group">
										<div class="col-md-6">
											<button type="submit" class="btn btn-default">
												<i class="fa fa-btn {{ $task->icon }}"></i>{{ $task->get_text() }}
											</button>
										</div>
									</div>
									
							</form>
					</div>
				
			</div>

		</div>
	</div>
@endsection