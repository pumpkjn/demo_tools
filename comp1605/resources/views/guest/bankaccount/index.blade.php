@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><strong>Hello, {{ $user->user_name }}</strong></h4>
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					@include('common.errors')
					<p>Address: {{ $user->address }}</p>
					<p>Phone: {{ $user->phone }}</p>
					<p>Passport: {{ $user->passport }}</p>
					<!-- New Task Form -->
				</div>
			</div>
			<!-- Current sites -->
			@if (count($accounts) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						Current Accounts
					</div>

					<div class="panel-body">
						<table class="table table-striped task-table">
							<thead>
								<th>Site</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
							<tr>
								<td class="table-text"><div>Branch Name</div></td>
								<td class="table-text"><div>Sort Code</div></td>
								<td class="table-text"><div>Account Number</div></td>	
								<td class="table-text"><div>IBAN</div></td>
								<td class="table-text"><div>Type</div></td>
								<td class="table-text"><div>Balance</div></td>
							</tr>
								@foreach ($accounts as $account)
									<tr id="display-site-{{ $account->id }}">
										<td class="table-text"><div><a href="{{  url('/bankaccount/account', $account->id ) }}">{{ $account->branchname }}</a></div></td>
										<td class="table-text"><div>{{ $account->sortcode }}</div></td>
										<td class="table-text"><div>{{ $account->accountnumber }}</div></td>
										<td class="table-text"><div>{{ $account->IBAN }}</div></td>
										<td class="table-text"><div>{{ $account->type }}</div></td>
										<td class="table-text"><div>{{ $account->balance }}</div></td>

									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4><strong>Tasks</strong></h4>
				</div>

				<div class="panel-body">
					<form action="{{ url('/site') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
						<div class="form-group">
							<div class="col-md-12">
								@foreach ($task->tasks as $t)
									<?php 
										$current_task = $task->create_task_class( $t );
									 ?>
									<a href="{{ url('/bankaccount/task', $current_task->view  ) }}" class="btn btn-default">
										<i class="fa fa-btn {{ $current_task->icon }}"></i>{{ $current_task->get_text() }}
									</a>
								@endforeach
							</div>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
@endsection