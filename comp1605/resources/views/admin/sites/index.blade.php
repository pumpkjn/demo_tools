@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
						New Site
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					@include('common.errors')

					<!-- New Task Form -->
					<form action="{{ url('/admin/site') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}

						<!-- Task Name -->
						<div class="form-group">
							<label for="site-name" class="col-sm-3 control-label">Packages</label>

							<div class="col-sm-6">
								 <!-- @yield('content') -->
								<!-- <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}"> -->
									@if ( !$packages->count() )
										You have no packages
									@else
									<?php   
										$p = array();
										foreach ($packages as $pack) {
											$p[ $pack->id ] = $pack->name;
										}
									  ?>
										{{ Form::select('package', $p, null, ['id' => 'package', 'class' => 'form-control']) }}
									@endif
									
							</div>
						</div>
						<div class="form-group">
							<label for="site-name" class="col-sm-3 control-label">Users</label>
							<div class="col-sm-6">
								@if ( !$users->count() )
										You have no users
								@else
								<?php   
									$u = array();
									foreach ($users as $user) {
										$u[ $user->id ] = $user->email;
									}
								  ?>
									{{ Form::select('user', $u, null, ['id' => 'user', 'class' => 'form-control']) }}
								@endif
							</div>
						</div>

						<!-- Add Task Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Add Site
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- Current sites -->
			
				<div class="panel panel-default">
					<div class="panel-heading sites-heading">
						Current sites
						<div class="sites-filters pull-right">
						    <?php 
						    ?>
							<div class="filter pull-right"> {{ Form::select('status-filter', $status_filter, $status_filter_default, ['id' => 'status-filter', 'class' => 'form-control', 'data-url' => route('sites.route')]) }} </div>
							<div class="filter pull-right"> {{ Form::text('search-box', $keywords, ['id' => 'search-box', 'class' => 'form-control', 'data-url' => route('sites.route')]) }} </div>
						</div>
					</div>
			@if (count($sites) > 0)	
					<div class="panel-body">
						<table class="table table-striped task-table">
							<thead>
								<th>Site</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
							<tr>
								<td class="table-text"><div>Name</div></td>
								<td class="table-text"><div>User Email</div></td>
								<td class="table-text"><div>Status</div></td>
								<td class="table-text"><div>Key</div></td>
								<td class="table-text"><div>Updated Date</div></td>
								<td class="table-text"><div>Actions</div></td>
								@foreach ($sites as $site)
									<tr id="display-site-{{ $site->id }}">
									<?php if ( '' == $status_filter_default ): ?>
										<td class="table-text"><div><a href="{{ url('/') }}/admin/sites?q={{ unserialize( $site->params )['package_name'] }}">{{ unserialize( $site->params )['package_name'] }}</a></div></td>
									<?php else: ?>
										<td class="table-text"><div><a href="{{ url('/') }}/admin/sites?q={{ unserialize( $site->params )['package_name'] }}&amp;status={{ $status_filter_default }}">{{ unserialize( $site->params )['package_name'] }}</a></div></td>
									<?php endif; ?>
										
										<td class="table-text"><div>{{ unserialize( $site->params )['user_email'] }}</div></td>
										<td class="site-status table-text"><div class="status-text">{{ $site->status }}</div></td>
										<td class="table-text"><div>{{ $site->key }}</div></td>
										<td class="table-text"><div>{{ $site->updated_at }}</div></td>
										<!-- Task Delete Button -->
										<td>
											<form action="{{ url('/') }}/admin/site/{{ $site->id }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}

												<button type="submit" id="delete-site-{{ $site->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="pull-left">
					<?php 
						if ( '' != $keywords  ) {
							if ( 'all' != $status_filter_default ) {
								echo $sites->appends(['q' => $keywords, 'status' => $status_filter_default])->render();
							} else {
								echo $sites->appends(['q' => $keywords])->render();
							}
						} else {
							if ( 'all' != $status_filter_default ) {
								echo $sites->appends(['status' => $status_filter_default])->render();
							} else {
								echo $sites->render();
							}
						}
					?>
                </div>
					@endif
				</div>
		</div>
	</div>
@endsection
