@extends('layouts.app')

@section('content')
	<div class="container">
	<div ><a href="/admin/users/add_user">Add User</a></div>
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading users-heading">
					All User
					<?php if ( empty( $keyword  ) || !$keyword  ) {
						$keyword = '';
					}
					?>
					<div id="user-search" class="pull-right" data-redirect={{ route('users.route') }} data-url="{{ $url }}">Search box <input type=text value="<?php echo $keyword ?>"></div>
				</div>
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/users/update_users') }}">
				{{ csrf_field() }}
				<div class="panel-body">
					<!-- Display Validation Errors -->
					@include('common.errors')
					<div class="panel panel-default">

			@if (count($users) > 0)	
					
						<table class="table table-striped task-table">
							<tbody>
							<tr>
								<td class="table-text"><div>Name</div></td>
								<td class="table-text"><div>User Email</div></td>
								<td class="table-text"><div>Updated Date</div></td>
								<td class="table-text"><div>Role</div></td>
								
								@foreach ($users as $user)
									<tr>
										<td class="table-text"><div>{{ $user->user_name }}</div></td>
										<td class="table-text"><div>{{ $user->email }}</div></td>
										<td class="table-text"><div>{{ $user->updated_at }}</div></td>
										<td class="table-text">
											<div>
												{{ Form::select( $user->user_name , $roles, $user->role_name, [ 'class' => 'role-select form-control' ]) }}
											</div>
										</td>
										
									</tr>
								@endforeach
							</tbody>
						</table>
						
					
					@endif
				</div>
				
				<div class="pull-left">
					<?php 
						if ( '' != $keyword ) {
							echo $users->appends(['q' => $keyword])->render();
						} else {
							echo $users->render();
						}
					?>
                </div>
				<div class="pull-right">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Update
                    </button>
                </div>
				</div>
			</div>
			</form>
		</div>
	</div>
@endsection
