@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php if ( empty( $keyword  ) || !$keyword  ) {
						$keyword = '';
					}
					?>
					<div id="package-search" class="pull-right" data-url="{{ route('packages.route') }}">Search box <input type=text value="<?php echo $keyword ?>"></div>
					All Packages
					<div class="filter">
					<?php $filters = array( 'all' => 'all', 'approved' => 'approved', 'unapproved' => 'unapproved' ) ?>
						{{ Form::select('package_filter', $filters, $filter_default, ['id' => 'package_filter', 'class' => 'form-control', 'data-url' => URL::to('/admin/packages') ]) }}
					</div>
					
				</div>

				@if (count($packages) > 0)
				<div class="panel-body">
					<div class="panel panel-default">
					<table class="package-table table table-striped task-table">
						<tbody>
							<td class="table-text"><div>Name</div></td>
							<td class="table-text"><div>Slug</div></td>   
							<td class="table-text"><div>Updated Date</div></td>
							<td class="table-text"><div>Actions</div></td>
							@foreach ($packages as $package)
								   <tr>
										<td id="package-{{ $package->id }}" data-package="{{ $package->id }}" data-token="<?php echo csrf_token(); ?>" class="table-text"><div><a href="{{ url('/') }}/admin/sites?package_id={{ $package->id }}">{{ $package->name }}</a><span class="package-edit pull-right"><i class="fa fa-pencil-square-o"></i><span></div></td>
										<td class="table-text"><div>{{ $package->slug }}</div></td>
										<td class="table-text"><div>{{ $package->updated_at }}</div></td>
										<!-- Task Delete Button -->
										<td>
											<form action="{{  url('/admin/package/status', $package->id ) }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('POST') }}
												@if ( 'approved' == $package->status )
												<button type="submit" id="approve" class="btn btn-danger">
													<i class="fa fa-btn fa-diamond"></i>Unapprove
												</button>
												@else
												<button type="submit" id="unapprove" class="btn btn-success">
													<i class="fa fa-btn fa-diamond"></i>Approve
												</button>
												@endif
											</form>
										</td>
									</tr>
							@endforeach
						<br>
						</tbody>
					</table>
					</div>
					<?php 
						if ( '' != $keyword  ) {
							if ( 'all' != $filter_default ) {
								echo $packages->appends(['q' => $keyword, 'filter' => $filter_default])->render();
							} else {
								echo $packages->appends(['q' => $keyword])->render();
							}
						} else {
							if ( 'all' != $filter_default ) {
								echo $packages->appends(['filter' => $filter_default])->render();
							} else {
								echo $packages->render();
							}
						}
					?>
				</div>
				@endif
			</div>
		</div>
	</div>

@endsection

