<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>COMP 1605</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
<div id="fb-root"></div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    COMP 1605 - Bank System
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (!Auth::guest())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->user_name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">

                            <?php if ( 1 == Auth::user()->role_id || 2 == Auth::user()->role_id ): ?>

                                <li><a href="{{ url('/admin/sites') }}"><i class="fa fa-btn fa-sign-out"></i>View Sites</a></li>

                                <li><a href="{{ url('/admin/packages_load') }}"><i class="fa fa-btn fa-sign-out"></i>View Packages</a></li>

                                <li><a href="{{ url('/system/send_mail') }}"><i class="fa fa-btn fa-sign-out"></i>Email send</a></li>

                                <li><a href="{{ url('/admin/users') }}"><i class="fa fa-btn fa-sign-out"></i>Manage User</a></li>
                                
                            <?php endif ?>

                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script src="{{ URL::asset('assets/js/packages.js') }}"></script>
    <?php if ( Auth::user() ): ?>
        <script type="text/javascript">
            jQuery(function($){
                var host = "{{ url('/') }}";
                var url = host + '/site/check';
                var interval = null;
                var timeout = false;

                $(document).on('ready',function(){
                    interval = setInterval(ajax_check_status,5000);
                });

                function ajax_check_status() {
                    $.ajax({
                        type:'GET',
                        dataType: "json",
                        url: url,
                        data: {
                            action : 'check_status'
                        },
                        success: function(response){
                            $.each( response, function( key, value ) {
                                var $site = $('#display-site-'+key);
                                $site.find('.site-status .status-text').text( value );
                            });
                        },
                        complete: function(xhr, textStatus) {
                            if ( xhr.status == 401 ) {
                                clearInterval(interval);
                            };
                            if ( xhr.responseText == 'empty' ) {
                                var sites = $('.site-status');
                                $.each( sites, function(){
                                    sites.find('.status-text').text( 'completed' );
                                })
                                clearInterval(interval);
                            };
                        } 
                    });
                };



            });



        </script>
    <?php endif ?>
    
    <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">

</body>
</html>
