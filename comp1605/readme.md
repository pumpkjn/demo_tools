# Laravel Project for control packages and sites

## Setup

1. Clone the project
2. From the command line, navigate to laravel directory >> Run 'composer install'
3. copy .env.example >> create .env file >> config ( Note*: APP Key sample : P2PnArHIfoWIno3F2c8WvTQrK5XPPcTp )
4. Run 'php artisan migrate'
5. Run 'php artisan db:seed'


## Feature

Checking route : Run 'php artisan route:list'

Refresh database : run 'php artisan migrate:refresh --seed'

## List of agruments:

### Databse & seeding :

	+ navigate to database 'database'

	+ Migrations: info of database

	+ Seeds: seed datas to database

		- DatabaseSeeder: sample users

		- RolesTableSeed: roles


### Packages controll :
	+ Navigate to : 'Storage/app/packages/' >> Place the packages .zip file

### Mail settings : 
	+ Navigate to : 'Config/mail.php' and in '.env' file

### Social API key( facebook, twitter, google plus, mandril ) :

	+Navigate to : 'Config/services.php'

	+ Callback route :
		- facebook: /facebook_callback
		- google:/google_plus_callback
		- twitter: /twitter_callback

### Additional routes:

	+ /system/send_mail in MailController : send request to action : send email.

	+ /user/update_email in UserController : update current user email.


### Javascripts & css files : 
	+ Navigate to : 'public/assets/...'



