@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					All Sites
				</div>
				@if (count($sites) > 0)
				<div class="panel-body">
					<table class="table table-striped task-table">
						<thead>
							<th>Packages</th>
							<th>&nbsp;</th>
						</thead>
						<tbody>
							<td class="table-text"><div>Name</div></td>
							<td class="table-text"><div>Status</div></td>   
							<td class="table-text"><div>Updated Date</div></td>
							<td class="table-text"><div>User</div></td>
							@foreach ($sites as $site)
								  <tr>
										<td class="table-text"><div>{{ unserialize( $site->params )['package_name'] }}</div></td>
										<td class="table-text"><div>{{ $site->status }}</div></td>
										<td class="table-text"><div>{{ $site->updated_at }}</div></td>
										<!-- Task Delete Button -->
										<td>
											<div>{{ $site->email }}</div>
										</td>
									</tr>
							@endforeach
						<br>
						</tbody>
					</table>
				</div>
				@endif
			</div>
		</div>
	</div>
@endsection