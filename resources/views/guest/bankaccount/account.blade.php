@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					Account Info
				</div>
	
				<div class="panel-body">
					<p>Branch Name: {{ $account->branchname }}</p>
					<p>Short Code: {{ $account->sortcode }}</p>
					<p>Account Number: {{ $account->accountnumber }}</p>
					<p>IBAN: {{ $account->IBAN }}</p>
					<p>Type: {{ $account->type }}</p>
					<p><strong>Balance: {{ $account->balance }}</strong></p>

				</div>
			</div>
			<!-- task 4 -->
			@if ( $account->type == "joinAccount" )
			<div class="panel panel-default">
				<div class="panel-heading">
					Customers info
				</div>
				<div class="panel-body">
					<div class="panel-heading">
						<?php $user = $userRepo->get_user_by_id( $account->user_id ) ?>
						Main User: {{ $user->user_name }} - {{ $user->address }}

						<?php $assocs = $account->associated; ?>
							@if ( $assocs )
							</br>
							Associated Customers : 
							</br>
								<?php $members = unserialize( $assocs ); ?>
								<ul>
								@foreach ( $members as $member ) 
									<?php $m = $userRepo->get_user_by_id( $member ); ?>
									<li>
										{{ $m->user_name }} - {{ $m->address }}
									</li>
								@endforeach
								</ul>
							@endif
					</div>
				</div>
			</div>
			@endif
			<!-- -->
			<div class="panel panel-default">
				<div class="panel-heading">
					Transactions
				</div>
	
				<div class="panel-body">
					<table class="table table-striped task-table">
							<tbody>
							<tr>
								<td class="table-text"><div>Type</div></td>
								<td class="table-text"><div>Amount</div></td>
								<td class="table-text"><div>From</div></td>	
								<td class="table-text"><div>To</div></td>
								<td class="table-text"><div>Balance</div></td>
								<td class="table-text"><div>Method</div></td>
								<td class="table-text"><div>Time</div></td>
							</tr>
								@foreach ($transactions as $tran)
									<tr id="display-site-{{ $tran->id }}">
										<td class="table-text"><div>{{ $tran->type }}</div></td>
										<td class="table-text"><div>{{ $tran->amount }}</div></td>
										<td class="table-text">
											<div>
												@if ( $account->get_bank_by_id( $tran->from ) )
													{{ $account->get_bank_by_id( $tran->from )->branchname }} - {{ $account->get_bank_by_id( $tran->from )->sortcode }} - {{ $account->get_bank_by_id( $tran->from )->accountnumber }} 
												@endif
											</div>
											</td>
										<td class="table-text"><div>
											@if ( $account->get_bank_by_id( $tran->to ) )
													{{ $account->get_bank_by_id( $tran->to )->branchname }} - {{ $account->get_bank_by_id( $tran->to )->sortcode }} - {{ $account->get_bank_by_id( $tran->to )->accountnumber }} 
												@endif
										</div></td>
										<td class="table-text"><div>{{ $tran->balance }}</div></td>
										<td class="table-text"><div>{{ $tran->method }}</div></td>
										<td class="table-text"><div>{{ $tran->created_at }}</div></td>
									</tr>
								@endforeach
							</tbody>
						</table>
				</div>
			</div>
		</div>

		<div class="col-sm-offset-1 col-sm-10">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Deposit
						</div>
						<?php $fail = isset( $_GET['status'] ) ? $_GET['status'] : null; ?>
						<div class="panel-body">
							<form action="{{ url('/bankaccount/account/deposit', $account->id) }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="col-md-6">
										Amount:<input name="deposit" value=""></input>
										<?php 
										if ( $fail && 'deposit_fail' == $fail ) {
											echo '<i>You need to input the deposit amount.</i>';
										} ?>
										<button type="submit" class="btn btn-default">
											<i class="fa fa-btn fa-plus"></i>Deposit
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Withdrawn
						</div>
			
						<div class="panel-body">
							<form action="{{ url('/bankaccount/account/withdraw', $account->id) }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="col-md-6">
										Amount:<input name="withdraw" value=""></input>
										<?php 
										if ( $fail && 'deposit_fail' == $fail ) {
											echo '</br><i>You need to input the deposit amount.</i>';
										} ?>
										<?php 
										if ( $fail && 'over_amount' == $fail ) {
											echo '</br><i>Amount of Withdraw need to be lower than your balance.</i>';
										} ?>
										<button type="submit" class="btn btn-default">
											<i class="fa fa-btn fa-minus"></i>Withdraw
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-sm-offset-1 col-sm-10">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							Transfer
						</div>
			
						<div class="panel-body">
							<form action="{{ url('/bankaccount/account/transfer', $account->id) }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="col-md-6">
										Transfer to:
										<select name="trans-bank">
											@foreach ($bankrepo as $bank)
												<?php if ( $bank->id != $account->id ): ?>
													<option value="{{ $bank->id }}">{{ $bank->branchname }} - {{ $bank->sortcode }} - {{ $bank->accountnumber }}</option>
												<?php endif ?>
											@endforeach
										</select>
										
									</div>
									<div class="col-md-6">
										Method:
										<select name="method">
											@foreach ($payment_method as $method)
												<option value="{{ $method }}">{{ $method }}</option>
											@endforeach
										</select>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										Amount:<input name="transfer" value=""></input>
										<?php 
										if ( $fail && 'transfer_fail' == $fail ) {
											echo '</br><i>You need to input the transfer amount.</i>';
										} ?>
										<?php 
										if ( $fail && 'over_amount_transfer' == $fail ) {
											echo '</br><i>Amount of transfer need to be lower than your balance.</i>';
										} ?>
										<button type="submit" class="btn btn-default">
											<i class="fa fa-btn fa-paper-plane"></i>Transfer
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection