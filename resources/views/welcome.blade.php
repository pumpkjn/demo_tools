@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 forms-block">
			<div class="row">
				<div class="col-md-6 social-block">
					<div class="logo" style="text-align: center">
						<img src="{{ url('/assets/img/t724.png') }}">
						<h3>The Doge Bank system</h3>
					</div>
					<div class="email-register collapse" id ="reg-mail">
						<h2> Email register </h2>
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
						                        {!! csrf_field() !!}

						                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						                            <label class="col-md-4 control-label">Name</label>

						                            <div class="col-md-8">
						                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

						                                @if ($errors->has('name'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('name') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						                            <label class="col-md-4 control-label">E-Mail Address</label>

						                            <div class="col-md-8">
						                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

						                                @if ($errors->has('email'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('email') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						                            <label class="col-md-4 control-label">Password</label>

						                            <div class="col-md-8">
						                                <input type="password" class="form-control" name="password">

						                                @if ($errors->has('password'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('password') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

						                            <label class="col-md-4 control-label">Confirm Password</label>

						                            <div class="col-md-8">
						                                <input type="password" class="form-control" name="password_confirmation">

						                                @if ($errors->has('password_confirmation'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group col-md-12">
						                            <div class="col-md-6 col-md-offset-4 btn-register">
						                                <button type="submit" class="btn btn-primary">
						                                    <i class="fa fa-btn fa-user"></i>Register
						                                </button>
						                            </div>
													<div class="col-md-2 btn-cancel">
														<a data-toggle="collapse" href="#reg-mail">Cancel</a>
													</div>
						                        </div>
						                    </form>

					</div>
				</div>

				<div class="col-md-6 login-block">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
						{!! csrf_field() !!}
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label class="control-label" for="email">E-Mail Address</label>
							<input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email">
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="control-label" for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password">
							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-10">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="remember"> Remember Me
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-btn fa-sign-in"></i>Login
									</button>
									<a class="btn btn-link forgot-pass" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
