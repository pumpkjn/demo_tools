<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'user_name' => 'ha07',
            'email' => 'ha07',
            'password' => bcrypt('ha07'),
            'passport' => 'ha070707',
            'phone' => '0777777777',
            'address' => '123 str London',
            'created_at' => new DateTime
        ]);

        DB::table('users')->insert([
            'user_name' => 'Allen',
            'email' => 'pumpkjn1508@gmail.com',
            'password' => bcrypt('admin'),
            'passport' => 'BA777777',
            'phone' => '0322322322',
            'address' => '322 str Doto',
            'created_at' => new DateTime
        ]);

        DB::table('users')->insert([
            'user_name' => 'user1',
            'email' => 'user1@gmail.com',
            'password' => bcrypt('test'),
            'passport' => 'BA777327',
            'phone' => '0322245122',
            'address' => '53 str Bank',
            'created_at' => new DateTime
        ]);
        DB::table('users')->insert([
            'user_name' => 'user2',
            'email' => 'user2@gmail.com',
            'password' => bcrypt('test'),
            'passport' => 'BA737327',
            'phone' => '0321245122',
            'address' => '54 str Broke',
            'created_at' => new DateTime
        ]);
        $this->call('BankAccountTablerSeeder');
    }
}
