<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->double('balance', 8, 2);;
            $table->string('sortcode');
            $table->string('branchname');
            $table->string('accountnumber');
            $table->string('IBAN');
            $table->string('type');
            $table->timestamps();

            // task 4
            $table->text('associated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
