# Laravel Project for my GW Master

## Setup

1. Clone the project
2. From the command line, navigate to laravel directory >> Run 'composer install'
3. copy .env.example >> create .env file >> config ( Note*: APP Key sample : P2PnArHIfoWIno3F2c8WvTQrK5XPPcTp )
4. Run 'php artisan migrate'
5. Run 'php artisan db:seed'


## Feature

Checking route : Run 'php artisan route:list'

Refresh database : run 'php artisan migrate:refresh --seed'

## List of agruments:


### Javascripts & css files : 
	+ Navigate to : 'public/assets/...'



